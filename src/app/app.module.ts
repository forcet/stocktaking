import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from '@shared/components/header/header.component';
import { FooterComponent } from '@shared/components/footer/footer.component';
import { MaterialModule } from '@app/material.module';
import { SidebarModule } from '@shared/components/sidebar/sidebar.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CheckloginGuard } from '@shared/guards/checklogin.guard';
import { AdminInterceptor } from '@shared/interceptor/admin-interceptor';
import { ReactiveFormsModule } from '@angular/forms';
import { CheckInternalGuard } from './shared/guards/check-internal.guard';
import { MessageComponent } from './shared/components/modal/message/message.component';
import { AlertsComponent } from './shared/components/modal/alerts/alerts.component';

const routes:  Routes = [
  { path: '', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) }, 
  { path: 'notFound', loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule) }, 
  { path: 'admin', loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminModule), canActivate:  [CheckInternalGuard] }, 
  { path: 'login', loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule), canActivate:  [CheckloginGuard] },
  { path: 'final', loadChildren: () => import('./pages/final/final.module').then(m => m.FinalModule), canActivate:  [CheckInternalGuard]  }
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MessageComponent,
    AlertsComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    SidebarModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AdminInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
