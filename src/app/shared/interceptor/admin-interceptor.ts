import { HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthService } from "@app/pages/auth/auth.service";
import { Observable } from "rxjs";

@Injectable()
export class AdminInterceptor implements HttpInterceptor {
    constructor(private auth: AuthService) {}
    intercept(req: HttpRequest<any>, next: HttpHandler) : Observable<any> {
        if(!req.url.includes('auth')) {
            const userLogin = this.auth.userValue;
            const authRequest = req.clone({
                setHeaders: {
                    Authorization: userLogin.body.token,
                },
            });
            return next.handle(authRequest);
        }
        
        return next.handle(req);
    }

}
