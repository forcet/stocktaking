export type Roles = 'NO_LOGIN' | 'USER' | 'ADMIN';

export interface LoginRequest {
    username: string;
    password: string;
}

export interface LoginResponse {
    result: string;
    body: {
        token: string;
        username: string;
        rol: Roles;
    }
}

export interface Person {
    username : string;
    password : string;
    status: string;
    rol: string;
    person: {
        legalId: string;
        legalIdType: string;
        names: string;
        lastName: string;
        birthDate: string;
        email: string;
        phone: string;
    }
}

export interface User {
    legalId: string;
    legalIdType: string;
    names: string;
    lastName: string;
    username : string;
    role: string;
    email: string;
    phone: string;
    birthDate: Date;
    creationDate: Date;
    updateDate: Date;
}

export interface Product {
    productId: string;
    username: string;
    name: string;
    realPrice: number;
    marketPrice: number;
    quantity: number;
    sellQuantity: number;
	releaseDate: string;
}


export interface Combo {
    value: string;
    description: string;
}

export interface Scheduler {
    userCode: string;
    productId: string;
    productName: string;
    nameScheduler: string;
    personalizeMessage: string;
    startDate: Date;
    endDate: Date;
    notNotifyMail: string;
    notNotifyPhone: string;
}

export interface Charts {
    name: string;
    value: number;
}

export interface Maintenance {
    maintenanceId: string;
    userCode: string;
    name: string;
    elaborateDate: string;
    expirationDate: string;
    cost : number;
    measurement : string;
    quantity : number;
    notifyUser: string;
    batchId: string;
}

export interface Animal {
    animalId : string;
    userCode: string;
    name: string;
    birthDate : string;
    codeMaintenance: string;
    cost: number;
    total: number;
    diaryConsume: string;
    notifyUser: string;
}

export interface Vegetable {
    vegetableId: string;
    userCode: string;
    name: string;
    buyDate: string;
    codeMaintenance: string;
    cost: number;
    total: number;
    diaryConsume: number;
    notifyUser: string

}

export interface Fixed {
    fixedId: string;
    userCode: string;
    name: string;
    quantities: string;
    status: string;
    cost: number;
}

export interface Batch {
    code: string;
    userCode: string;
    name: string;
    description: string;
    type: string;
}

export interface File {
    url: string;
    name: string;
}


