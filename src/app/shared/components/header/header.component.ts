import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { UtilServices } from '@app/shared/services/utils.services';
import { AuthService } from '@auth/auth.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  isAdmin = 'NO_LOGIN';
  isLogged = false;

  private destroy$ = new Subject<any>(); 

  @Output() toggleSidenav = new EventEmitter<void>();
  constructor(private authSvc: AuthService, 
    private utilService: UtilServices
    ) { }

  ngOnInit(): void {

    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.isLogged = res?true:false;
      this.isAdmin = res?res.body.rol:'NO_LOGIN';
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  onToggleSidenav() {
    this.toggleSidenav.emit();
  }

  onLogout() {
    this.utilService.openSideBar(false);
    this.authSvc.logout();
  }

}
