import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '@app/pages/auth/auth.service';
import { UtilServices } from '@app/shared/services/utils.services';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {


  isAdmin = "NO_LOGIN";
  isLogged = false;

  panelOpenState = false;

  private destroy$ = new Subject<any>(); 

  constructor(private authSvc: AuthService, private utilService: UtilServices) { }  

  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.isLogged = res?true:false;
      this.isAdmin = res?res.body.rol:'NO_LOGIN';
    });
  }

  onExit(): void {
    this.authSvc.logout();
    this.utilService.openSideBar(false);
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

}
