import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar.component';
import { MaterialModule } from '@app/material.module';
import { RouterModule } from '@angular/router';
import { UtilServices } from '@app/shared/services/utils.services';



@NgModule({
  declarations: [
    SidebarComponent
  ],
  imports: [
    CommonModule, MaterialModule, RouterModule
  ],
  exports: [SidebarComponent],
  providers: [UtilServices]
})
export class SidebarModule { }
