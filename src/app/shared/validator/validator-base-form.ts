import { FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class ValidatorBase {

    constructor() {}
  
    isValidField(field: string, fb: FormGroup): boolean {
      //this.getErrorMessage(field);
      return (
        (fb.get(field)!.touched || fb.get(field)!.dirty) &&
        !fb.get(field)!.valid
      );
    }
  
     getErrorMessage(field: string, fb: FormGroup): string {
      const { errors } = fb.get(field)!;
      if (errors) {
        const minlenght = errors?.minlength?.requiredLength;
        const maxlenght = errors?.maxlength?.requiredLength;
        const minNumber = errors?.min?.min;
        const messages: any = {
          required: 'Debe ingresar un valor.',
          pattern: 'Valor no valido.',
          minlength: `El campo debe tener más de ${minlenght} caracteres`,
          maxlength: `El campo debe tener menos de ${maxlenght} caracteres`,
          min: `El campo debe ser mayor que ${minNumber}`,
        };
        const errorKey = Object.keys(errors).find(Boolean);
        return messages[errorKey!];
      }
      return "";
    }    

}