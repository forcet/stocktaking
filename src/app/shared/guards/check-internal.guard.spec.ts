import { TestBed } from '@angular/core/testing';

import { CheckInternalGuard } from './check-internal.guard';

describe('CheckInternalGuard', () => {
  let guard: CheckInternalGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CheckInternalGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
