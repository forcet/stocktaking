import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '@app/pages/auth/auth.service';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { LoginResponse } from '../models/user.interface';

@Injectable({
  providedIn: 'root'
})
export class CheckloginGuard implements CanActivate {
  
  constructor(private auth: AuthService) {}
  
  canActivate(): Observable<boolean> {
    return this.auth.user$
      .pipe(
          take(1), 
          map((user: LoginResponse) => (!user ? true : false))
          );
  }
  
}
