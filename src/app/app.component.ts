import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UtilServices } from './shared/services/utils.services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'stocktaking';
  opened = false;

  private destroy$ = new Subject<any>();

  constructor(private util: UtilServices) {}

  ngOnInit(): void {
    this.util.sideBarOpened$
    .pipe(takeUntil(this.destroy$))
    .subscribe((res: boolean) =>  this.opened = res)
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

}
