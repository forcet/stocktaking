import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FixedComponent } from './fixed.component';
import { MaterialModule } from '@app/material.module';
import { FixedViewComponent } from './components/fixed-view/fixed-view.component';
import { ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  { path: '', component: FixedComponent }
];

@NgModule({
  declarations: [
    FixedComponent,
    FixedViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class FixedModule { }
