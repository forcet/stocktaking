import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '@app/pages/auth/auth.service';
import { FixedService } from '@app/pages/final/services/fixed.service';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Fixed } from '@app/shared/models/user.interface';
import { ValidatorBase } from '@app/shared/validator/validator-base-form';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

enum Action {
  EDIT = 'edit',
  NEW = 'new'
}

@Component({
  selector: 'app-fixed-view',
  templateUrl: './fixed-view.component.html',
  styleUrls: ['./fixed-view.component.css']
})
export class FixedViewComponent implements OnInit {


  messageOk = "Su transaccion se ha completado correctamente";
  generalAction = Action.NEW;
  private destroy$ = new Subject<any>();

  usercode!: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private validator : ValidatorBase, 
    public fb: FormBuilder, 
    private fixedSvc : FixedService, 
    private authSvc: AuthService, 
    private dialog: MatDialog
  ) { }

  formFixed = this.fb.group({
    fixedId: ['', [Validators.required]],
    name: ['', [Validators.required]],
    quantities: ['',[Validators.required]],
    status: ['',[Validators.required]],
    cost: ['',[Validators.required]]
  });

  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.usercode = res.body.username;
    });
    if(this.data?.fixed.hasOwnProperty('fixedId')) {
      this.generalAction = Action.EDIT;
      this.data.title = 'Editar Herramientas';
      this.pathFormData();
    }
    

    
  }


  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  onSave(): void {
    const formValue = this.formFixed.value;
    
    let vegetables : Fixed = { 
      fixedId: formValue.fixedId,
      userCode: this.usercode,
      name :formValue.name,
      quantities: formValue.quantities,
      status : formValue.status,
      cost: formValue.cost
    }

    if(this.generalAction === Action.NEW) {
      this.fixedSvc.newFixed(vegetables).subscribe((res) => this.showDialogInformation(res) , (err) => this.showDialogInformation(err) );
    } else {
      this.fixedSvc.updateFixed(vegetables).subscribe((res) => this.showDialogInformation(res), (err) => this.showDialogInformation(err));
    }
  }


  checkField(field: string): boolean {
    return this.validator.isValidField(field, this.formFixed);
  }

  getErrors(field : string): string {
    return this.validator.getErrorMessage(field, this.formFixed);
  }


  showDialogInformation(res : any):void {
    let message = res.status == 200? this.messageOk: res.error;
    this.messageInformation(message);
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }

  private pathFormData(): void {

    this.formFixed.patchValue({
      fixedId: this.data?.fixed?.fixedId,
      name: this.data?.fixed?.name,
      quantities: this.data?.fixed?.quantities,
      status: this.data?.fixed?.status,
      cost: this.data?.fixed?.cost
    });
  }

}
