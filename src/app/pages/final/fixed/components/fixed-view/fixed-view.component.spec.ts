import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedViewComponent } from './fixed-view.component';

describe('FixedViewComponent', () => {
  let component: FixedViewComponent;
  let fixture: ComponentFixture<FixedViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FixedViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
