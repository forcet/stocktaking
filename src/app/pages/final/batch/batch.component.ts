import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from '@app/pages/auth/auth.service';
import { AlertsComponent } from '@app/shared/components/modal/alerts/alerts.component';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BatchService } from '../services/batch.service';
import { BatchViewComponent } from './components/batch-view/batch-view.component';

@Component({
  selector: 'app-batch',
  templateUrl: './batch.component.html',
  styleUrls: ['./batch.component.css']
})
export class BatchComponent implements OnInit, OnDestroy {

  private destroy$ = new Subject<any>();

  usercode!: string;
  
  constructor(private dialog: MatDialog, private batchSvc : BatchService, private authSvc: AuthService) { }
  

  displayedColumns: string[] = ['code', 'name', 'description', 'type', 'actions'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator, {static: false}) 
  paginator: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.usercode = res.body.username;
    });
    this.refresh();
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }


  onDelete(batch : any):void {
    this.dialog.open(AlertsComponent, {
      hasBackdrop: false,
      data: {title: 'Eliminar Calendarización', code: batch.code}
    }).afterClosed().subscribe((res) => {
        if(res) { 
          this.batchSvc
            .deleteBatch(batch.id)
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => this.refresh(), (err) => this.messageInformation(err.error));
        }
    });   
  }


  onOpenModal(batch = {}): void {
    let modal =this.dialog.open(BatchViewComponent, {
      height:'670px',
      width:'600px',
      hasBackdrop: false,
      data: {title: 'Nuevo Lote', batch}
    });
    modal.afterClosed().subscribe(() => this.refresh());
  }

  onCreateMasive(): void {
    this.messageInformation("Correcto");
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }

  refresh(): void {
    this.batchSvc.getAllBatch(this.usercode).subscribe((res) => {
      this.dataSource = new MatTableDataSource<any>(res); 
      this.dataSource.paginator = this.paginator; 
    }, (err) => this.messageInformation(err.error));
  }

}
