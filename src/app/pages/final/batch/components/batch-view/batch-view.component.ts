import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '@app/pages/auth/auth.service';
import { BatchService } from '@app/pages/final/services/batch.service';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Batch } from '@app/shared/models/user.interface';
import { ValidatorBase } from '@app/shared/validator/validator-base-form';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

enum Action {
  EDIT = 'edit',
  NEW = 'new'
}

@Component({
  selector: 'app-batch-view',
  templateUrl: './batch-view.component.html',
  styleUrls: ['./batch-view.component.css']
})
export class BatchViewComponent implements OnInit {

  messageOk = "Su transaccion se ha completado correctamente";
  generalAction = Action.NEW;
  private destroy$ = new Subject<any>();

  usercode!: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private validator : ValidatorBase, 
    public fb: FormBuilder, 
    private batchSvc : BatchService, 
    private authSvc: AuthService, 
    private dialog: MatDialog) { }

    batchForm = this.fb.group({
      code: ['', [Validators.required]],
      name: ['', [Validators.required]],
      description: ['',[Validators.required]],
      type:  ['',[Validators.required]]
      
    });

  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.usercode = res.body.username;
    });
    if(this.data?.batch.hasOwnProperty('code')) {
      this.generalAction = Action.EDIT;
      this.data.title = 'Editar Lote';
      this.pathFormData();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  checkField(field: string): boolean {
    return this.validator.isValidField(field, this.batchForm);
  }

  getErrors(field : string): string {
    return this.validator.getErrorMessage(field, this.batchForm);
  }

  onSave(): void {
    const formValue = this.batchForm.value;
    let batch : Batch = { 
      userCode: this.usercode,
      code: formValue.code,
      name: formValue.name,
      description: formValue.description,
      type: formValue.type
    }
    if(this.generalAction === Action.NEW) {
      this.batchSvc.newBatch(batch).subscribe((res) => this.showDialogInformation(res) , (err) => this.showDialogInformation(err) );
    } else {
      this.batchSvc.updateBatch(batch).subscribe((res) => this.showDialogInformation(res), (err) => this.showDialogInformation(err));
    }
  }


  showDialogInformation(res : any):void {
    let message = res.status == 200? this.messageOk: res.error;
    this.messageInformation(message);
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }

  private pathFormData(): void {

    this.batchForm.patchValue({
        code: this.data?.batch?.code,
        name: this.data?.batch?.name,
        description: this.data?.batch?.description,
        type: this.data?.batch?.type
    });
  }

}
