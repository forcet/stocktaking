import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BatchComponent } from './batch.component';
import { BatchViewComponent } from './components/batch-view/batch-view.component';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  { path: '', component: BatchComponent }
];

@NgModule({
  declarations: [
    BatchComponent,
    BatchViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class BatchModule { }
