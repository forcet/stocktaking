import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from '@app/pages/auth/auth.service';
import { AlertsComponent } from '@app/shared/components/modal/alerts/alerts.component';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Scheduler } from '@app/shared/models/user.interface';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SchedulerService } from '../services/scheduler.service';
import { ViewComponent } from './components/view/view.component';

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.component.html',
  styleUrls: ['./scheduler.component.css']
})
export class SchedulerComponent implements OnInit, OnDestroy {


  private destroy$ = new Subject<any>();

  usercode!: string;
  
  constructor(private dialog: MatDialog, private schedSvc : SchedulerService, private authSvc: AuthService) { }
  

  displayedColumns: string[] = ['productId', 'productName', 'startDate', 'endDate', 'notNotifyMail', 'notNotifyPhone', 'personalizeMessage', 'actions'];
  dataSource = new MatTableDataSource<Scheduler>();

  @ViewChild(MatPaginator, {static: false}) 
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.usercode = res.body.username;
    });
    this.refresh();
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }


  onDelete(scheduler : any):void {
    this.dialog.open(AlertsComponent, {
      hasBackdrop: false,
      data: {title: 'Eliminar Calendarización', code: scheduler.productId}
    }).afterClosed().subscribe((res) => {
        if(res) { 
          this.schedSvc
            .deleteScheduler(scheduler.id)
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => this.refresh(), (err) => this.messageInformation(err.error));
        }
    });   
  }


  onOpenModal(scheduler = {}): void {
    let modal =this.dialog.open(ViewComponent, {
      height:'670px',
      width:'600px',
      hasBackdrop: false,
      data: {title: 'Nuevo Calendarización', scheduler}
    });
    modal.afterClosed().subscribe(() => this.refresh());
  }

  onCreateMasive(): void {
    this.messageInformation("Correcto");
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }

  refresh(): void {
    this.schedSvc.getAllSchedulerUser(this.usercode).subscribe((res) => {
      this.dataSource = new MatTableDataSource<Scheduler>(res); 
      this.dataSource.paginator = this.paginator; 
    }, (err) => this.messageInformation(err.error));
  }
}
