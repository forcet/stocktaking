import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '@app/pages/auth/auth.service';
import { ProductsService } from '@app/pages/final/services/products.service';
import { SchedulerService } from '@app/pages/final/services/scheduler.service';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Combo, Scheduler } from '@app/shared/models/user.interface';
import { ValidatorBase } from '@app/shared/validator/validator-base-form';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

enum Action {
  EDIT = 'edit',
  NEW = 'new'
}

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit, OnDestroy {

  messageOk = "Su transaccion se ha completado correctamente";
  generalAction = Action.NEW;
  private destroy$ = new Subject<any>();

  usercode!: string;

  products: Combo[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private validator : ValidatorBase, 
    public fb: FormBuilder, 
    private schedSvc : SchedulerService, 
    private prodSvc : ProductsService, 
    private authSvc: AuthService, 
    private dialog: MatDialog) { }

    schedulerForm = this.fb.group({
      productId: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      nameScheduler: ['',[Validators.required]],
      endDate: [],
      notNotifyMail: [],
      notNotifyPhone: [],
      personalizeMessage: ['', [Validators.required]]
      
    });

  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.usercode = res.body.username;
    });
    if(this.data?.scheduler.hasOwnProperty('productId')) {
      this.generalAction = Action.EDIT;
      this.data.title = 'Editar Calendarizacion';
      this.pathFormData();
    }
    this.getListProduct(this.usercode);
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  checkField(field: string): boolean {
    return this.validator.isValidField(field, this.schedulerForm);
  }

  getErrors(field : string): string {
    return this.validator.getErrorMessage(field, this.schedulerForm);
  }

  onSave(): void {
    const formValue = this.schedulerForm.value;
    let scheduler : Scheduler = { 
      userCode: this.usercode,
      productId: formValue.productId,
      productName: formValue.productName,
      nameScheduler: formValue.nameScheduler,
      startDate: formValue.startDate,
      endDate: formValue.endDate,
      notNotifyMail: formValue.notNotifyMail,
      notNotifyPhone: formValue.notNotifyPhone,
      personalizeMessage: formValue.personalizeMessage
    }
    if(this.generalAction === Action.NEW) {
      this.schedSvc.newScheduler(scheduler).subscribe((res) => this.showDialogInformation(res) , (err) => this.showDialogInformation(err) );
    } else {
      this.schedSvc.updateScheduler(scheduler).subscribe((res) => this.showDialogInformation(res), (err) => this.showDialogInformation(err));
    }
  }

  getListProduct(code: string){
    this.prodSvc.getAllProducts(code).subscribe((res) => {
      const combo = res.map((product : any) => {
        const object: Combo = {
          value: product.productId, 
          description: product.productName
      };
        return object;
      }); 
      this.products = combo;
    });
  }

  showDialogInformation(res : any):void {
    let message = res.status == 200? this.messageOk: res.error;
    this.messageInformation(message);
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }

  private pathFormData(): void {
    let startDate = this.data?.scheduler?.startDate;
    startDate = startDate != null ? this.formatDate(startDate, '-') : startDate;
    let endDate = this.data?.scheduler?.endDate;
    endDate = endDate != null ? this.formatDate(endDate, '-') : endDate;
    this.schedulerForm.patchValue({
        productId: this.data?.scheduler?.productId,
        productName: this.data?.scheduler?.productName,
        nameScheduler: this.data?.scheduler?.nameScheduler,
        startDate: startDate,
        endDate: endDate,
        notNotifyMail: this.data?.scheduler?.notNotifyMail,
        notNotifyPhone: this.data?.scheduler?.notNotifyPhone,
        personalizeMessage: this.data?.scheduler?.personalizeMessage
    });
  }

  private formatDate(date: any, separator: string): string {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + (d.getDate());
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join(separator);
  }

}
