import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from '@app/pages/auth/auth.service';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpeciesService {

  constructor(private client: HttpClient) {  }


  findAllSpecieAnimalUser(userCode: string) : Observable<any> {
    return this.client
                .get<any>(`${environment.API_URL}/specieAnimal/findAllSpecieAnimalByUser?userCode=${userCode}`)
                .pipe(map((res) =>  res.listBody), catchError(this.handlerError));
  }

  findAllSpecieVegetableUser(userCode: string) : Observable<any> {
    return this.client
                .get<any>(`${environment.API_URL}/specieVegetable/findAllSpecieVegetableByUser?userCode=${userCode}`)
                .pipe(map((res) => res.listBody), catchError(this.handlerError));
  }

  newSpecieAnimal(specieAnimal: any) : Observable<any> {
    return this.client
                .post<any>(`${environment.API_URL}/specieAnimal/saveSpecieAnimal`, specieAnimal)
                .pipe(catchError(this.handlerError));
  }

  newSpecieVegetable(specieVegetable: any) : Observable<any> {
    return this.client
                .post<any>(`${environment.API_URL}/specieVegetable/saveSpecieVegetable`, specieVegetable)
                .pipe(catchError(this.handlerError));
  }

  updateSpecieAnimal(specieAnimal: any) : Observable<any> {
    return this.client
                .patch<any>(`${environment.API_URL}/specieAnimal/updateSpecieAnimal`, specieAnimal)
                .pipe(catchError(this.handlerError));
  }

  updateSpecieVegetable(specieVegetable: any) : Observable<any> {
    return this.client
                .patch<any>(`${environment.API_URL}/specieVegetable/updateSpecieVegetable`, specieVegetable)
                .pipe(catchError(this.handlerError));
  }

  deleteSpecieAnimal(specieAnimalId: string) : Observable<any> {
    return this.client
                .delete<any>(`${environment.API_URL}/specieAnimal/deleteSpecieAnimal?specieAnimalId=${specieAnimalId}`)
                .pipe(catchError(this.handlerError));
  }

  deleteSpecieVegetable(specieVegetableId: string) : Observable<any> {
    return this.client
                .delete<any>(`${environment.API_URL}/specieVegetable/deleteSpecieVegetable?specieVegetableId=${specieVegetableId}`)
                .pipe(catchError(this.handlerError));
  }

  generateSpecialAnimalArchiveData(file: File, code : string) : Observable<HttpEvent<any>> {
    const formData : FormData = new FormData();
    formData.append('code', code);
    formData.append('files', file);
    const req = new HttpRequest('POST', `${environment.API_URL}/specieAnimal/uploadSpecieAnimal`, formData, {
      reportProgress: true,
      responseType: 'json'
    })
    return this.client
                .request(req)
                .pipe(catchError(this.handlerError));
  }

  generateSpecialVegetableArchiveData(file: File, code : string) : Observable<HttpEvent<any>> {
    const formData : FormData = new FormData();
    formData.append('code', code);
    formData.append('files', file);
    const req = new HttpRequest('POST', `${environment.API_URL}/specieVegetable/uploadSpecieVegetable`, formData, {
      reportProgress: true,
      responseType: 'json'
    })
    return this.client
                .request(req)
                .pipe(catchError(this.handlerError));
  }

  private handlerError(err: { error: any; }): Observable<never> {
    console.log(err);
    return throwError(err.error);
  }
}
