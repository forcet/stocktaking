import { TestBed } from '@angular/core/testing';

import { FixedService } from './fixed.service';

describe('FixedService', () => {
  let service: FixedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FixedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
