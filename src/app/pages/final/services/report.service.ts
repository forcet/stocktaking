import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private client: HttpClient) { }

  getReport(code: string, table: string): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.client.get<any>(`${environment.API_URL}/reports/report?code=${code}&table=${table}`, { headers, responseType: 'blob' as 'json'})
                      .pipe(catchError(this.handlerError));;
  }

  getCharInfo(code: string, table: string): Observable<any> {
    return this.client.get<any>(`${environment.API_URL}/reports/chart?code=${code}&table=${table}`)
                      .pipe(catchError(this.handlerError));;
  }

  getFiles(): Observable<any> {
    return this.client.get<any>(`${environment.API_URL}/files/files?code=upload`)
    .pipe(map((res) => { return res.body; }), catchError(this.handlerError));
  }

  getFile(name : string): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.client.get<any>(`${environment.API_URL}/files/file?file=${name}&code=upload`, { headers, responseType: 'blob' as 'json'})
                      .pipe(catchError(this.handlerError));
  }


  private handlerError(err: { error: any; }): Observable<never> {
    return throwError(err.error);
  }
}
