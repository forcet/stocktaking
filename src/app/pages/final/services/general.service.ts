import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  constructor(private client: HttpClient) { }


  getRecap(code: string) : Observable<any> {
    return this.client
                .get<any>(`${environment.API_URL}/recap/findRecapByUser?userCode=${code}`)
                .pipe(map((res) => res.body), catchError(this.handlerError));
  }

  private handlerError(err: { error: any; }): Observable<never> {
    return throwError(err.error);
  }

}
