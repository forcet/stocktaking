import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FixedService {

  constructor(private client: HttpClient) { }

  getAllFixed(code: string) : Observable<any> {
    return this.client
                .get<any>(`${environment.API_URL}/fixed/findAllFixedByUser?userCode=${code}`)
                .pipe(map((res) => res.listBody), catchError(this.handlerError));
  }


  newFixed(fixed: any) : Observable<any> {
    return this.client
                .post<any>(`${environment.API_URL}/fixed/saveFixed`, fixed)
                .pipe(catchError(this.handlerError));
  }

  updateFixed(fixed: any) : Observable<any> {
    return this.client
                .patch<any>(`${environment.API_URL}/fixed/updateFixed`, fixed)
                .pipe(catchError(this.handlerError));
  }

  deleteFixed(fixedId: string) : Observable<any> {
    return this.client
                .delete<any>(`${environment.API_URL}/fixed/deleteFixed?fixedId=${fixedId}`)
                .pipe(catchError(this.handlerError));
  }

  generateArchiveData(file: File, code : string) : Observable<HttpEvent<any>> {
    const formData : FormData = new FormData();
    formData.append('code', code);
    formData.append('files', file);
    const req = new HttpRequest('POST', `${environment.API_URL}/fixed/uploadFixed`, formData, {
      reportProgress: true,
      responseType: 'json'
    })
    return this.client
                .request(req)
                .pipe(catchError(this.handlerError));
  }

  
  private handlerError(err: { error: any; }): Observable<never> {
    return throwError(err.error);
  }
}
