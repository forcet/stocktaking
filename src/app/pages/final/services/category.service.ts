import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private client: HttpClient) { }

  getAllMaintenance(code: string) : Observable<any> {
    return this.client
                .get<any>(`${environment.API_URL}/maintenance/findAllMaintenanceByUser?userCode=${code}`)
                .pipe(map((res) => res.listBody), catchError(this.handlerError));
  }


  newMaintenance(maintenance: any) : Observable<any> {
    return this.client
                .post<any>(`${environment.API_URL}/maintenance/saveMaintenance`, maintenance)
                .pipe(catchError(this.handlerError));
  }

  updateMaintenance(maintenance: any) : Observable<any> {
    return this.client
                .patch<any>(`${environment.API_URL}/maintenance/updateMaintenance`, maintenance)
                .pipe(catchError(this.handlerError));
  }

  deleteMaintenance(maintenanceId: string) : Observable<any> {
    return this.client
                .delete<any>(`${environment.API_URL}/maintenance/deleteMaintenance?maintenanceId=${maintenanceId}`)
                .pipe(catchError(this.handlerError));
  }

  generateArchiveData(file: File, code : string) : Observable<HttpEvent<any>> {
    const formData : FormData = new FormData();
    formData.append('code', code);
    formData.append('files', file);
    const req = new HttpRequest('POST', `${environment.API_URL}/maintenance/uploadMaintenance`, formData, {
      reportProgress: true,
      responseType: 'json'
    })
    return this.client
                .request(req)
                .pipe(catchError(this.handlerError));
  }

  
  private handlerError(err: { error: any; }): Observable<never> {
    return throwError(err.error);
  }

}
