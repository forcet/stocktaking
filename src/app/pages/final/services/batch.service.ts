import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BatchService {

  constructor(private client: HttpClient) { }


  getAllBatch(code: string) : Observable<any> {
    return this.client
                .get<any>(`${environment.API_URL}/batch/findAllBatchByUser?userCode=${code}`)
                .pipe(map((res) => res.listBody), catchError(this.handlerError));
  }

  newBatch(scheduler: any) : Observable<any> {
    return this.client
                .post<any>(`${environment.API_URL}/batch/saveBatch`, scheduler)
                .pipe(catchError(this.handlerError));
  }

  updateBatch(scheduler: any) : Observable<any> {
    return this.client
                .patch<any>(`${environment.API_URL}/batch/updateBatch`, scheduler)
                .pipe(catchError(this.handlerError));
  }

  deleteBatch(code: string) : Observable<any> {
    return this.client
                .delete<any>(`${environment.API_URL}/batch/deleteBatch?code=${code}`)
                .pipe(catchError(this.handlerError));
  }

  private handlerError(err: { error: any; }): Observable<never> {
    return throwError(err.error);
  }

}
