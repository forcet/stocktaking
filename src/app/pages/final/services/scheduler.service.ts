import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SchedulerService {

  constructor(private client: HttpClient) { }


  getAllSchedulerUser(code: string) : Observable<any> {
    return this.client
                .get<any>(`${environment.API_URL}/scheduler/findAllSchedulerByUser?code=${code}`)
                .pipe(map((res) => res.listBody), catchError(this.handlerError));
  }

  newScheduler(scheduler: any) : Observable<any> {
    return this.client
                .post<any>(`${environment.API_URL}/scheduler/saveScheduler`, scheduler)
                .pipe(catchError(this.handlerError));
  }

  updateScheduler(scheduler: any) : Observable<any> {
    return this.client
                .patch<any>(`${environment.API_URL}/scheduler/updateScheduler`, scheduler)
                .pipe(catchError(this.handlerError));
  }

  deleteScheduler(code: string) : Observable<any> {
    return this.client
                .delete<any>(`${environment.API_URL}/scheduler/deleteScheduler?code=${code}`)
                .pipe(catchError(this.handlerError));
  }

  private handlerError(err: { error: any; }): Observable<never> {
    return throwError(err.error);
  }

}
