import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private client: HttpClient) { }

  getAllProducts(code: string) : Observable<any> {
    return this.client
                .get<any>(`${environment.API_URL}/products/findAllProductsByUser?userCode=${code}`)
                .pipe(map((res) => res.listBody), catchError(this.handlerError));
  }

  generateArchiveData(file: File, code : string) : Observable<HttpEvent<any>> {
    const formData : FormData = new FormData();
    formData.append('code', code);
    formData.append('files', file);
    const req = new HttpRequest('POST', `${environment.API_URL}/products/uploadProduct`, formData, {
      reportProgress: true,
      responseType: 'json'
    })
    return this.client
                .request(req)
                .pipe(catchError(this.handlerError));
  }


  getUserInfo(username: string) : Observable<any> {
    return this.client
                .get<any>(environment.API_URL)
                .pipe(catchError(this.handlerError));
  }


  newProduct(product: any) : Observable<any> {
    return this.client
                .post<any>(`${environment.API_URL}/products/saveProduct`, product)
                .pipe(catchError(this.handlerError));
  }

  updateProduct(product: any) : Observable<any> {
    return this.client
                .patch<any>(`${environment.API_URL}/products/updateProduct`, product)
                .pipe(catchError(this.handlerError));
  }

  deleteProduct(productCode: string) : Observable<any> {
    return this.client
                .delete<any>(`${environment.API_URL}/products/deleteProduct?productId=${productCode}`)
                .pipe(catchError(this.handlerError));
  }

  private handlerError(err: { error: any; }): Observable<never> {
    return throwError(err.error);
  }

}
