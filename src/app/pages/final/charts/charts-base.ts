import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Charts } from '@app/shared/models/user.interface';
import { ReportService } from '../services/report.service';


@Injectable({providedIn: 'root'})
export class ChartBase {

    single : Charts[] = [];
    colorScheme : any = {};
    chart : string = "" ;
  
    view: [number, number] = [1000, 600];
    legend: boolean = true;
    legendPosition: any = 'below';
  
    gradient: boolean = false;
    animations: boolean = true;
    showLabels: boolean = true;
    isDoughnut: boolean = false;
    showLegend: boolean = true;
    cardColor: string = '#232837';
  
    showXAxis = true;
    showYAxis = true;
    showXAxisLabel = true;
    xAxisLabel = 'Producto';
    showYAxisLabel = true;
    yAxisLabel = 'Cantidad';



    constructor(private reportSvc: ReportService, private dialog: MatDialog) {}


    onGenerateDataChart(graphic : string, userCode: string): void {
        this.chart = graphic;
        this.reportSvc.getCharInfo(userCode,  '').subscribe((res) => {
            this.single = this.generateData(res.body.data);
            this.colorScheme = this.generateColor(res.body.colors);
        }, (err) => this.resultMessage(err.error));
      }

      generateColor(response : any) : any {
        return {domain : response};
      }
    
      generateData(response : any): Charts[] {
        return response.map((res : any) => {
          const object: Charts = {
            name: res.name, 
            value: res.value
        };
          return object;
        }); 
      }

      resultMessage(message: string): void {
        this.dialog.open(MessageComponent, {
          data: {message}
        });
      }

    

}
