import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Charts } from '@app/shared/models/user.interface';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { GeneralService } from './services/general.service';
import { ReportService } from './services/report.service';

@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.css']
})
export class FinalComponent implements OnInit, OnDestroy {

  private destroy$ = new Subject<any>();

  usercode!: string;

  single: Charts[] = [];
  colorScheme: any = {};
  view: [number, number] = [600, 600];

  legend: boolean = true;
  legendPosition: any = 'below';

  gradient: boolean = false;
  animations: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  showLegend: boolean = true;
  cardColor: string = '#232837';

  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  xAxisLabel = 'Producto';
  showYAxisLabel = true;
  yAxisLabel = 'Cantidad';

  items : any = [];

  constructor(
    private dialog: MatDialog, 
    private authSvc: AuthService,
    private reportSvc: ReportService
  ) { }


  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.usercode = res.body.username;
    });
    this.onGenerateDataChart();
    this.items = this.getFiles();
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  onGenerateDataChart(): void {
    this.reportSvc.getCharInfo(this.usercode, 'maintenance').subscribe((res) => {
      this.single = this.generateData(res.body.data);
      this.colorScheme = this.generateColor(res.body.colors);
    }, (err) => this.resultMessage(err.error));
  }

  generateColor(response: any): any {
    return { domain: response };
  }

  generateData(response: any): Charts[] {
    return response.map((res: any) => {
      const object: Charts = {
        name: res.name,
        value: res.value
      };
      return object;
    });
  }

  resultMessage(message: string): void {
    this.dialog.open(MessageComponent, {
      data: { message }
    });
  }

  getFiles() : void {
    this.reportSvc.getFiles().subscribe((res) => {
      this.items = res;
    });
    
  }

  onServiceClick(name: string) : void {
      this.reportSvc.getFile(name).subscribe((res)=> {
        this.manageFile(res, name);  
      });
  }

  manageFile(response: any, fileName: string): void {
    const dataType = response.type;
    const binaryData = [];
    binaryData.push(response);
    const filtePath = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
    const downloadLink = document.createElement('a');
    downloadLink.href = filtePath;
    downloadLink.setAttribute('download', fileName);
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

  labelFormatting(c: any) {
    return `${(c.label)} Population`;
  }

}
