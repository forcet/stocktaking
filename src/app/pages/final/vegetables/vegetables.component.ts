import { HttpResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from '@app/pages/auth/auth.service';
import { AlertsComponent } from '@app/shared/components/modal/alerts/alerts.component';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Charts } from '@app/shared/models/user.interface';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ReportService } from '../services/report.service';
import { SpeciesService } from '../services/species.service';
import { VegetableViewComponent } from './components/vegetable-view/vegetable-view.component';

@Component({
  selector: 'app-vegetables',
  templateUrl: './vegetables.component.html',
  styleUrls: ['./vegetables.component.css']
})
export class VegetablesComponent implements OnInit {

  messageOk = "Su transaccion se ha completado correctamente";

  private destroy$ = new Subject<any>();

  usercode!: string;

  constructor(
    private dialog: MatDialog,
    private spcdSvc: SpeciesService,
    private authSvc: AuthService,
    private reportSvc: ReportService
  ) { }

  displayedColumns: string[] = ['vegetableId', 'name', 'buyDate', 'codeMaintenanceDescription', 'cost', 'total', 'diaryConsume', 'actions'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator, { static: false })
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }


  ngOnInit(): void {
    this.authSvc.user$
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        this.usercode = res.body.username;
      });
      this.onGenerateDataChart('');
      this.refresh();
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }


  onOpenModal(vegetables = {}): void {
    let modal = this.dialog.open(VegetableViewComponent, {
      height: '670px',
      width: '600px',
      hasBackdrop: false,
      data: { title: 'Nuevo Especie ', vegetables }
    });
    modal.afterClosed().subscribe(() => this.refresh());
  }

  onDelete(vegetables: any): void {
    this.dialog.open(AlertsComponent, {
      hasBackdrop: false,
      data: { title: 'Eliminar Especie', code: vegetables.vegetableId }
    }).afterClosed().subscribe((res) => {
      if (res) {
        this.spcdSvc
          .deleteSpecieVegetable(vegetables.id)
          .pipe(takeUntil(this.destroy$))
          .subscribe(() => this.refresh(), (err) => this.messageInformation(err.error));
      }
    });
  }

  refresh(): void {
    this.spcdSvc.findAllSpecieVegetableUser(this.usercode).subscribe((res) => {
      this.dataSource = new MatTableDataSource<any>(res);
      this.dataSource.paginator = this.paginator;
    }, (err) => this.messageInformation(err.error));
  }

  messageInformation(message: string): void {
    this.dialog.open(MessageComponent, {
      data: { message }
    })
  }

  single: Charts[] = [];
  colorScheme: any = {};
  chart: string = "";

  view: [number, number] = [700, 600];
  legend: boolean = true;
  legendPosition: any = 'below';

  gradient: boolean = false;
  animations: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  showLegend: boolean = true;
  cardColor: string = '#232837';

  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  xAxisLabel = 'Producto';
  showYAxisLabel = true;
  yAxisLabel = 'Cantidad';




  onGenerateDataChart(graphic: string): void {
    this.chart = graphic;
    this.reportSvc.getCharInfo(this.usercode, 'specieVegetable').subscribe((res) => {
      this.single = this.generateData(res.body.data);
      this.colorScheme = this.generateColor(res.body.colors);
    }, (err) => this.resultMessage(err.error));
  }

  generateColor(response: any): any {
    return { domain: response };
  }

  generateData(response: any): Charts[] {
    return response.map((res: any) => {
      const object: Charts = {
        name: res.name,
        value: res.value
      };
      return object;
    });
  }

  resultMessage(message: string): void {
    this.dialog.open(MessageComponent, {
      data: { message }
    });
  }

  labelFormatting(c: any) {
    return `${(c.label)} Population`;
  }

  selectedFiles!: FileList;
  fileName = '';
  progressInfo : any[] = [];
  isActivate : boolean = false;


  onArchive(event: any): void {
    this.progressInfo = [];
    if (event.target.files.length == 1) {
      this.fileName = event.target.files[0].name;
    } else {
      this.fileName = event.target.files.length + ' archivos';
    }
    this.selectedFiles = event.target.files;
    this.isActivate = true;
  }

  uploadFiles(): void {
    for(let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(i, this.selectedFiles[i]);
    }
  }

  upload(index : number, file: any) {
    const object = {value: 0, fileName: file.name };  
    this.progressInfo[index] = object;
    this.spcdSvc.generateSpecialVegetableArchiveData(file, this.usercode)
              .pipe(takeUntil(this.destroy$))
              .subscribe((res) =>  {
                if(res instanceof HttpResponse) {
                  this.messageInformation('Su transaccion se ha completado correctamente');
                  this.refresh(); 
                  this.isActivate = false;
                  this.fileName = '';
                }
              }, () => this.messageInformation('Ha ocurrido un error'));                          
  }




  onGenerateReport(): void {
    const fileName = `${this.usercode}_productos_${Math.random()}.xlsx`;
    this.reportSvc.getReport(this.usercode, 'specieVegetable').subscribe((res) => {
      this.manageExcelFile(res, fileName);
    }, (err) => this.resultMessage(err.error));
  }

  manageExcelFile(response: any, fileName: string): void {
    const dataType = response.type;
    const binaryData = [];
    binaryData.push(response);

    const filtePath = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
    const downloadLink = document.createElement('a');
    downloadLink.href = filtePath;
    downloadLink.setAttribute('download', fileName);
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }

}
