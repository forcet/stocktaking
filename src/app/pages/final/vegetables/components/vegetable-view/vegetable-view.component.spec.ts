import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VegetableViewComponent } from './vegetable-view.component';

describe('VegetableViewComponent', () => {
  let component: VegetableViewComponent;
  let fixture: ComponentFixture<VegetableViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VegetableViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VegetableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
