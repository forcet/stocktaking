import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { VegetablesComponent } from './vegetables.component';
import { VegetableViewComponent } from './components/vegetable-view/vegetable-view.component';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  { path: '', component: VegetablesComponent }
];

@NgModule({
  declarations: [
    VegetablesComponent,
    VegetableViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class VegetablesModule { }
