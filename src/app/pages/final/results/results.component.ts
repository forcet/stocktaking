import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from '@app/pages/auth/auth.service';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Charts } from '@app/shared/models/user.interface';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GeneralService } from '../services/general.service';
import { ReportService } from '../services/report.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit, OnDestroy {

  messageOk = "Su transaccion se ha completado correctamente";

  private destroy$ = new Subject<any>();

  usercode!: string;

  constructor(
    private dialog: MatDialog,
    private generaldSvc: GeneralService,
    private authSvc: AuthService,
    private reportSvc: ReportService
  ) { }

  displayedColumns: string[] = ['batchId', 'name', 'quality', 'qualityActual', 'cost'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator, { static: false })
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }


  ngOnInit(): void {
    this.authSvc.user$
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        this.usercode = res.body.username;
      });
    this.onGenerateDataChart('');
    this.refresh();
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }


  refresh(): void {
    this.generaldSvc.getRecap(this.usercode).subscribe((res) => {
      this.dataSource = new MatTableDataSource<any>(res);
      this.dataSource.paginator = this.paginator;
    }, (err) => { this.messageInformation(err.error)});
  }

  messageInformation(message: string): void {
    this.dialog.open(MessageComponent, {
      data: { message }
    })
  }


  single: Charts[] = [];
  colorScheme: any = {};
  chart: string = "";

  view: [number, number] = [700, 600];
  legend: boolean = true;
  legendPosition: any = 'below';

  gradient: boolean = false;
  animations: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  showLegend: boolean = true;
  cardColor: string = '#232837';

  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  xAxisLabel = 'Producto';
  showYAxisLabel = true;
  yAxisLabel = 'Cantidad';




  onGenerateDataChart(graphic: string): void {
    this.chart = graphic;
    this.reportSvc.getCharInfo(this.usercode, 'result').subscribe((res) => {
      this.single = this.generateData(res.body.data);
      this.colorScheme = this.generateColor(res.body.colors);
    }, (err) => this.resultMessage(err.error));
  }

  generateColor(response: any): any {
    return { domain: response };
  }

  generateData(response: any): Charts[] {
    return response.map((res: any) => {
      const object: Charts = {
        name: res.name,
        value: res.value
      };
      return object;
    });
  }

  resultMessage(message: string): void {
    this.dialog.open(MessageComponent, {
      data: { message }
    });
  }

  labelFormatting(c: any) {
    return `${(c.label)} Population`;
  }


  selectedFiles!: FileList;
  fileName = '';
  progressInfo : any[] = [];
  isActivate : boolean = false;


  onArchive(event: any): void {
    this.progressInfo = [];
    if (event.target.files.length == 1) {
      this.fileName = event.target.files[0].name;
    } else {
      this.fileName = event.target.files.length + ' archivos';
    }
    this.selectedFiles = event.target.files;
    this.isActivate = true;
  }

}
