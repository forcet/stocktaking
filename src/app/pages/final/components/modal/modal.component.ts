import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '@app/pages/auth/auth.service';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Combo, Product } from '@app/shared/models/user.interface';
import { ValidatorBase } from '@app/shared/validator/validator-base-form';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CategoryService } from '../../services/category.service';
import { ProductsService } from '../../services/products.service';

enum Action {
  EDIT = 'edit',
  NEW = 'new'
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, OnDestroy {

  messageOk = "Su transaccion se ha completado correctamente";
  generalAction = Action.NEW;
  private destroy$ = new Subject<any>();

  usercode!: string;

  family: Combo[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private validator : ValidatorBase, 
    public fb: FormBuilder, 
    private prodSvc : ProductsService, 
    private authSvc: AuthService, 
    private dialog: MatDialog
    ) { }


  productForm = this.fb.group({
    productId: ['', [Validators.required, Validators.minLength(3)]],
    name: ['', [Validators.required]],
    realPrice: ['', [Validators.required, Validators.min(0)]],
    marketPrice: ['', [Validators.required, Validators.min(0)]],
    quantity: ['', [Validators.required, Validators.min(0)]],
    sellQuantity: ['', [Validators.required]],
    releaseDate: ['', [Validators.required]]
    
  });

  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.usercode = res.body.username;
    });
    if(this.data?.product.hasOwnProperty('productId')) {
      this.generalAction = Action.EDIT;
      this.data.title = 'Editar Articulo';
      this.pathFormData();
    }
  }


  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  onSave(): void {
    const formValue = this.productForm.value;
    let product : Product = { 
      productId: formValue.productId,
      username: this.usercode,
      name: formValue.name,
      realPrice: formValue.realPrice,
      marketPrice: formValue.marketPrice,
      quantity: formValue.quantity,
      sellQuantity: formValue.sellQuantity,
      releaseDate: this.formatDate(formValue.releaseDate, '/')
    }

    if(this.generalAction === Action.NEW) {
      this.prodSvc.newProduct(product).subscribe((res) => this.showDialogInformation(res) , (err) => this.showDialogInformation(err) );
    } else {
      this.prodSvc.updateProduct(product).subscribe((res) => this.showDialogInformation(res), (err) => this.showDialogInformation(err));
    }

  }

  private formatDate(date: any, separator: string): string {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join(separator);
  }

  showDialogInformation(res : any):void {
    let message = res.status == 200? this.messageOk: res.error;
    this.messageInformation(message);
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }

  checkField(field: string): boolean {
    return this.validator.isValidField(field, this.productForm);
  }

  getErrors(field : string): string {
    return this.validator.getErrorMessage(field, this.productForm);
  }

  private pathFormData(): void {
    let releaseDate = this.formatDate(this.data?.product?.releaseDate, '-');
    this.productForm.patchValue({
        productId: this.data?.product?.productId,
        name: this.data?.product?.name,
        realPrice: this.data?.product?.realPrice,
        quantity: this.data?.product?.quantity,
        marketPrice: this.data?.product?.marketPrice,
        pquantityrice: this.data?.product?.pquantityrice,
        sellQuantity: this.data?.product?.sellQuantity,
        releaseDate: releaseDate

    });
  }

}
