import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FinalComponent } from './final.component';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CheckInternalGuard } from '@app/shared/guards/check-internal.guard';
import { ModalComponent } from './components/modal/modal.component';


const routes: Routes = [
  { path: '', component: FinalComponent },
  { path: 'products', loadChildren: () => import('./products/products.module').then(m => m.ProductsModule), canActivate:  [CheckInternalGuard] },
  { path: 'fixed', loadChildren: () => import('./fixed/fixed.module').then(m => m.FixedModule), canActivate:  [CheckInternalGuard] },
  { path: 'scheduler', loadChildren: () => import('./scheduler/scheduler.module').then(m => m.SchedulerModule), canActivate:  [CheckInternalGuard] },
  { path: 'maintenance', loadChildren: () => import('./maintenance/maintenace.module').then(m => m.MaintenaceModule), canActivate:  [CheckInternalGuard] },
  { path: 'animals', loadChildren: () => import('./animals/animals.module').then(m => m.AnimalsModule), canActivate:  [CheckInternalGuard] },
  { path: 'vegetables', loadChildren: () => import('./vegetables/vegetables.module').then(m => m.VegetablesModule), canActivate:  [CheckInternalGuard] },
  { path: 'batch', loadChildren: () => import('./batch/batch.module').then(m => m.BatchModule), canActivate:  [CheckInternalGuard] },
  { path: 'results', loadChildren: () => import('./results/results.module').then(m => m.ResultsModule), canActivate:  [CheckInternalGuard] }
];

@NgModule({
  declarations: [
    FinalComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class FinalModule { }
