import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '@app/pages/auth/auth.service';
import { BatchService } from '@app/pages/final/services/batch.service';
import { CategoryService } from '@app/pages/final/services/category.service';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Combo, Maintenance } from '@app/shared/models/user.interface';
import { ValidatorBase } from '@app/shared/validator/validator-base-form';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


enum Action {
  EDIT = 'edit',
  NEW = 'new'
}

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.css']
})
export class CategoryViewComponent implements OnInit, OnDestroy {

  messageOk = "Su transaccion se ha completado correctamente";
  generalAction = Action.NEW;
  private destroy$ = new Subject<any>();

  usercode!: string;

  batchs: Combo[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private validator : ValidatorBase, 
    public fb: FormBuilder, 
    private catdSvc : CategoryService, 
    private authSvc: AuthService, 
    private batchSvc : BatchService,
    private dialog: MatDialog
  ) { }

  formCategory = this.fb.group({
    maintenanceId: ['', [Validators.required]],
    name: ['', [Validators.required]],
    elaborateDate: ['',[Validators.required]],
    expirationDate: ['',[Validators.required]],
    cost: ['',[Validators.required]],
    measurement: ['',[Validators.required]],
    quantity: ['',[Validators.required]],
    notifyUser: ['',[Validators.required]],
    loteId: ['',[Validators.required]]
  });

  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.usercode = res.body.username;
    });
    if(this.data?.category.hasOwnProperty('maintenanceId')) {
      this.generalAction = Action.EDIT;
      this.data.title = 'Editar Categoria';
      this.pathFormData();
    }
    this.getBatchList(this.usercode);
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  onSave(): void {
    const formValue = this.formCategory.value;
    
    let category : Maintenance = { 
      maintenanceId: formValue.maintenanceId,
      userCode: this.usercode,
      name :formValue.name,
      elaborateDate: this.formatDate(formValue.elaborateDate, '/'),
      expirationDate: this.formatDate(formValue.expirationDate, '/'),
      cost: formValue.cost,
      measurement : formValue.measurement,
      quantity : formValue.quantity,
      notifyUser : formValue.notifyUser,
      batchId: formValue.loteId
    }

    if(this.generalAction === Action.NEW) {
      this.catdSvc.newMaintenance(category).subscribe((res) => this.showDialogInformation(res) , (err) => this.showDialogInformation(err) );
    } else {
      this.catdSvc.updateMaintenance(category).subscribe((res) => this.showDialogInformation(res), (err) => this.showDialogInformation(err));
    }
  }

  private formatDate(date: any, separator: string): string {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join(separator);
  }

  checkField(field: string): boolean {
    return this.validator.isValidField(field, this.formCategory);
  }

  getErrors(field : string): string {
    return this.validator.getErrorMessage(field, this.formCategory);
  }


  showDialogInformation(res : any):void {
    let message = res.status == 200? this.messageOk: res.error;
    this.messageInformation(message);
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }

  private pathFormData(): void {
    let dateInit = this.formatDate(this.data?.category?.elaborateDate, '-');
    let dateExp = this.formatDate(this.data?.category?.expirationDate, '-');
    this.formCategory.patchValue({
      maintenanceId: this.data?.category?.maintenanceId,
      name: this.data?.category?.name,
      elaborateDate: dateInit,
      expirationDate: dateExp,
      cost: this.data?.category?.cost,
      measurement: this.data?.category?.measurement,
      quantity: this.data?.category?.quantity,
      notifyUser: this.data?.category?.notifyUser,
      loteId: this.data?.category?.batchId

    });
  }

  getBatchList(code: string){
    this.batchSvc.getAllBatch(code).subscribe((res) => {
      const combo = res.map((batch : any) => {
        const object: Combo = {
          value: batch.code, 
          description: batch.name
      };
        return object;
      }); 
      this.batchs = combo;
    });
  }
}
