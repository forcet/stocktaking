import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MaintenaceComponent } from './maintenace.component';
import { CategoryViewComponent } from './components/category-view/category-view.component';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  { path: '', component: MaintenaceComponent }
];

@NgModule({
  declarations: [
    MaintenaceComponent,
    CategoryViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class MaintenaceModule { }
