import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AnimalsComponent } from './animals.component';
import { AnimalViewComponent } from './components/animal-view/animal-view.component';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  { path: '', component: AnimalsComponent }
];

@NgModule({
  declarations: [
    AnimalsComponent,
    AnimalViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class AnimalsModule { }
