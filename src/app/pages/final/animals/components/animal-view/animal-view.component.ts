import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '@app/pages/auth/auth.service';
import { CategoryService } from '@app/pages/final/services/category.service';
import { SpeciesService } from '@app/pages/final/services/species.service';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Animal, Combo } from '@app/shared/models/user.interface';
import { ValidatorBase } from '@app/shared/validator/validator-base-form';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

enum Action {
  EDIT = 'edit',
  NEW = 'new'
}

@Component({
  selector: 'app-animal-view',
  templateUrl: './animal-view.component.html',
  styleUrls: ['./animal-view.component.css']
})
export class AnimalViewComponent implements OnInit, OnDestroy {


  messageOk = "Su transaccion se ha completado correctamente";
  generalAction = Action.NEW;
  private destroy$ = new Subject<any>();

  usercode!: string;

  family: Combo[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private validator : ValidatorBase, 
    public fb: FormBuilder, 
    private spcSvc : SpeciesService, 
    private authSvc: AuthService, 
    private dialog: MatDialog,
    private catSvc: CategoryService
  ) { }

  formAnimals = this.fb.group({
    animalId: ['', [Validators.required]],
    name: ['', [Validators.required]],
    birthDate: ['',[Validators.required]],
    codeMaintenance: ['',[Validators.required]],
    cost: ['',[Validators.required]],
    total: ['',[Validators.required]],
    diaryConsume: ['',[Validators.required]],
    notifyUser: ['',[Validators.required]]
  });



  ngOnInit(): void {
    this.authSvc.user$
    .pipe(takeUntil(this.destroy$))
    .subscribe(res => {
      this.usercode = res.body.username;
    });
    if(this.data?.animals.hasOwnProperty('animalId')) {
      this.generalAction = Action.EDIT;
      this.data.title = 'Editar Animal';
      this.pathFormData();
    }
    this.getListCategory(this.usercode);

  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  onSave(): void {
    const formValue = this.formAnimals.value;
    
    let animals : Animal = { 
      animalId: formValue.animalId,
      userCode: this.usercode,
      name :formValue.name,
      birthDate: this.formatDate(formValue.birthDate, '/'),
      codeMaintenance: formValue.codeMaintenance,
      cost: formValue.cost,
      total : formValue.total,
      diaryConsume : formValue.diaryConsume,
      notifyUser : formValue.notifyUser
    }

    if(this.generalAction === Action.NEW) {
      this.spcSvc.newSpecieAnimal(animals).subscribe((res) => this.showDialogInformation(res) , (err) => this.showDialogInformation(err) );
    } else {
      this.spcSvc.updateSpecieAnimal(animals).subscribe((res) => this.showDialogInformation(res), (err) => this.showDialogInformation(err));
    }
  }


  private formatDate(date: any, separator: string): string {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join(separator);
  }

  checkField(field: string): boolean {
    return this.validator.isValidField(field, this.formAnimals);
  }

  getErrors(field : string): string {
    return this.validator.getErrorMessage(field, this.formAnimals);
  }


  showDialogInformation(res : any):void {
    let message = res.status == 200? this.messageOk: res.error;
    this.messageInformation(message);
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }

  getListCategory(code: string){
    this.catSvc.getAllMaintenance(code).subscribe((res) => {
      const combo = res.map((category : any) => {
        const object: Combo = {
          value: category.maintenanceId, 
          description: category.name
      };
        return object;
      }); 
      this.family = combo;
    });
  }

  private pathFormData(): void {
    let birthDate = this.formatDate(this.data?.animals?.birthDate, '-');

    this.formAnimals.patchValue({
      animalId: this.data?.animals?.animalId,
      name: this.data?.animals?.name,
      birthDate: birthDate,
      codeMaintenance: this.data?.animals?.codeMaintenance,
      cost: this.data?.animals?.cost,
      total: this.data?.animals?.total,
      diaryConsume: this.data?.animals?.diaryConsume,
      notifyUser : this.data?.animals?.notifyUser

    });
  }

}
