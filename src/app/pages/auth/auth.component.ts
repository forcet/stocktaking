import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '@auth/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ValidatorBase } from '@app/shared/validator/validator-base-form';
import { MatDialog } from '@angular/material/dialog';
import { NewViewComponent } from './components/new-view/new-view.component';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {

  hide = true;
  private destroy$ = new Subject<any>(); 

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', [ Validators.required, Validators.minLength(6) ]]
  })

  constructor(private authSvc: AuthService, 
              private fb: FormBuilder, 
              private router: Router, 
              private validator : ValidatorBase,
              private dialog: MatDialog) { }
  
  ngOnInit(): void { }

  onLogin() {
    if(this.loginForm.invalid) {
      return
    }
    const formValue = this.loginForm.value;

    this.authSvc.login(formValue)
    .pipe(takeUntil(this.destroy$))
    .subscribe((res) => {
      if(res && res.body.rol=='ADMIN') {
        this.router.navigate(['admin']);
      } else {
        this.router.navigate(['final']);
      }
    }, () => this.messageInformation('Por Zeus!!!! Usuario o contraseña Incorrectos!!!'));
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  getErrorMessage(field: string): string {
    return this.validator.getErrorMessage(field, this.loginForm);
  }

  isValidField(field: string) {
    return this.validator.isValidField(field, this.loginForm);
  }

  onOpenModal(): void {
    this.dialog.open(NewViewComponent, {
      height: '700px',
      width: '800px',
      //hasBackdrop: false,
      data: { title: 'Registrar Nuevo Usuario ' }
    });
  }

}
