import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Person } from '@app/shared/models/user.interface';
import { ValidatorBase } from '@app/shared/validator/validator-base-form';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-new-view',
  templateUrl: './new-view.component.html',
  styleUrls: ['./new-view.component.css']
})
export class NewViewComponent implements OnInit, OnDestroy {

  selected = 'C';

  private destroy$ = new Subject<any>(); 

  messageOk = 'Usuario generado correctamente. Esta listo para ingresar.';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private validator : ValidatorBase, 
    public fb: FormBuilder, 
    private authSvc: AuthService,
    private dialog: MatDialog
  ) { }

  isLinear = true;
  isValidEmail = /\S+@\S+\.\S+/;
  isNumeric = /^([0-9])*$/;

  personForm = this.fb.group({
    name: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    birthDate: ['',[Validators.required]],
    legalIdType: ['',[Validators.required]],
    legalId: ['',[Validators.required]],
    email: ['',[Validators.required, Validators.pattern(this.isValidEmail)]],
    phone: ['',[Validators.required, Validators.pattern(this.isNumeric)]]
  });

  userForm = this.fb.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required, Validators.minLength(6)]]
  });

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }



  onSave(): void {
    const personValue = this.personForm.value;
    const userValue = this.userForm.value;
    let newUser : Person = {
      username: userValue.username,
      password: userValue.password,
      status: '1',
      rol: 'FINAL',
      person: {
        legalId: personValue.legalId,
        legalIdType: personValue.legalIdType,
        names: personValue.name,
        lastName: personValue.lastName,
        birthDate: this.formatDate(personValue.birthDate, '/'),
        email: personValue.email,
        phone: personValue.phone
      }
    } 

    this.authSvc.createUser(newUser)
            .subscribe((res) => { this.showDialogInformation(res);  } , 
            (err) => this.showDialogInformation(err) );

  }

  showDialogInformation(res : any):void {
    let message = res.status == 200? this.messageOk: res.error;
    this.messageInformation(message);
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }



  checkField(field: string, value : string): boolean {
    if("person" == value) {
      return this.validator.isValidField(field, this.personForm);
    } else {
      return this.validator.isValidField(field, this.userForm);
    }
    
  }

  getErrors(field : string, value : any): string {
    if("person" == value) {
      return this.validator.getErrorMessage(field, this.personForm);
    } else {
      return this.validator.getErrorMessage(field, this.userForm);
    }
  }

  private formatDate(date: any, separator: string): string {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join(separator);
  }

}
