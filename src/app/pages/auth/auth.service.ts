import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginResponse, LoginRequest, Roles } from '@shared/models/user.interface';
import { environment } from '@env/environment';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient, private router: Router) { this.checkToken(); }

  get user$(): Observable<LoginResponse> {
    return this.user.asObservable();
  }

  get userValue(): LoginResponse {
    return this.user.getValue();
  }

  login(authData: LoginRequest): Observable<LoginResponse | void> {
    return this.http
    .post<LoginResponse>(`${environment.API_URL}/auth`, authData)
    .pipe( 
      map ( (res: LoginResponse) => {
        this.saveLocalStorage(res);
        this.user.next(res);
        return res;
    }), catchError ( (error) => this.handlerError(error)));
  }

  createUser(user: any) : Observable<any> {
    let create = { result: "ok", body:  { token: "1" }};
    this.user.next(create);
    return this.http
                .post<any>(`${environment.API_URL}/users/saveUser`, user)
                .pipe(map (
                  (res) => {this.user.next(null); return res; }
                ), catchError(this.handlerError));
  }
  
  logout() {
    localStorage.removeItem('user');
    this.user.next(null);
    this.router.navigate(['/login'])
  }

  public checkToken() {
    console.log("isCheckToken");
    const userString = localStorage.getItem('user') || null;
    if(userString) {
      const user = JSON.parse(userString);
      const isExpired = helper.isTokenExpired(user.body.token);
      console.log("isExpired -", isExpired);
      if(isExpired) {
        this.logout();
      } else {
        this.user.next(user);
      }
    } else {
      console.log("here -");
      this.logout();
    } 
  }

  private saveLocalStorage(user : LoginResponse) {
    const {result, ... rest} = user;
    localStorage.setItem('user', JSON.stringify(rest));
  }

  private handlerError(err: { error: any; }): Observable<never> {
    return throwError(err.error);
  }
}
