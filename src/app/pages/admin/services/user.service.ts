import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Person, User } from '@app/shared/models/user.interface';
import { environment } from '@env/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private client: HttpClient) { }

  getAll(): Observable<any> {
    return this.client
                .get<any>(`${environment.API_URL}/users/findAll`) 
                .pipe(map((res) => {
                  const person = res.listBody.map((user : any) => {
                    const object: User = {
                      username: user.username, 
                      legalIdType: user.person.legalIdType,
                      legalId: user.person.legalId,
                      names: user.person.names,
                      lastName: user.person.lastName,
                      role: user.rol,
                      email: user.person.email,
                      phone: user.person.phone,
                      birthDate: user.person.birthDate,
                      creationDate: user.creationDate,
                      updateDate: user.updateDate
                  };
                    return object;
                  });
                  return person;
                } ), catchError(this.handlerError));

  }

  getById(): Observable<any> {
    return this.client
                .get<any>(environment.API_URL)
                .pipe(catchError(this.handlerError));
  }

  new(user: any): Observable<any> {
    return this.client
                .post<any>(`${environment.API_URL}/users/saveUser`, user)
                .pipe(catchError(this.handlerError));
  }

  update(user: any): Observable<any> {
    return this.client
              .patch<any>(`${environment.API_URL}/users/updateUser`, user)
              .pipe(catchError(this.handlerError));
  }

  delete(username: string): Observable<any> {
    return this.client
                .delete<any>(`${environment.API_URL}/users/deleteUser?codeUser=${username}`)
                .pipe(catchError(this.handlerError));;
  }

  private handlerError(err: { message: any; }): Observable<never> {
    let errorMessage = 'An errror occured retrienving data';
    if (err) {
      errorMessage = `Error: code ${err.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
