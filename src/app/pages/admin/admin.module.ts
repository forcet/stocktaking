import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ModalComponent } from './components/modal/modal.component';
import { MaterialModule } from '@app/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CheckInternalGuard } from '@app/shared/guards/check-internal.guard';


const routes: Routes = [
  { path: '', component: AdminComponent },
  { path: 'users', loadChildren: () => import('./users/users.module').then(m => m.UsersModule), canActivate:  [CheckInternalGuard] }
];

@NgModule({
  declarations: [
    AdminComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
