import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageComponent } from '@app/shared/components/modal/message/message.component';
import { Person } from '@app/shared/models/user.interface';
import { ValidatorBase } from '@app/shared/validator/validator-base-form';
import { Subject } from 'rxjs';
import { UserService } from '../../services/user.service';



enum Action {
  EDIT = 'edit',
  NEW = 'new'
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, OnDestroy {

  messageOk = "Su transaccion se ha completado correctamente";
  generalAction = Action.NEW;
  showPassword = true;
  hide = true;

  private destroy$ = new Subject<any>(); 

  constructor(@Inject(MAT_DIALOG_DATA) public data : any, private validator : ValidatorBase, private userSvc : UserService, public fb: FormBuilder, private dialog: MatDialog) { }

  private isValidEmail = /\S+@\S+\.\S+/;

  userForm = this.fb.group({
    username: ['', [Validators.required, Validators.minLength(5)]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    role: ['', [Validators.required]],
    legalIdType: ['', [Validators.required]],
    legalId: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
    names: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.pattern(this.isValidEmail)]],
    phone: ['', [Validators.required]],
    birthDate: ['', [Validators.required]]
  
  });


  ngOnInit(): void {
    if(this.data?.user.hasOwnProperty('username')) {
      this.generalAction = Action.EDIT;
      this.showPassword = false;
      this.userForm.get('password')?.setValidators(null);
      this.userForm.updateValueAndValidity();
      this.data.title = 'Editar Usuario';
      this.pathFormData();
    }

  }

  onSave(): void {
    const formValue = this.userForm.value;
    let person : Person = { 
      username: formValue.username,
      password: formValue.password?formValue.password:"1", 
      status: '1,',
      rol: formValue.role,
      person: {
        legalId: formValue.legalId,
        legalIdType: formValue.legalIdType,
        names: formValue.names,
        lastName: formValue.lastName,
        birthDate : this.formatDate(formValue.birthDate, '/'),
        email: formValue.email,
        phone: formValue.phone
      }
    };

    if(this.generalAction === Action.NEW) {
      this.userSvc.new(person).subscribe((res) => this.showMesageTransation(res));
    } else {
      this.userSvc.update(person).subscribe((res) => this.showMesageTransation(res));
    }
  }

  showMesageTransation(res : any): void {
    let message = res.status == 200? this.messageOk: res.message;
    this.messageInformation(message);
  }

  messageInformation(message : string): void {
    this.dialog.open(MessageComponent, {
      data: {message}
    })
  }


  checkField(field: string): boolean {
    return this.validator.isValidField(field, this.userForm);
  }

  getErrors(field : string): string {
    return this.validator.getErrorMessage(field, this.userForm);
  }

  private pathFormData(): void {
    let birthDay = this.data?.user?.birthDate;
    this.userForm.patchValue({
        username: this.data?.user?.username,
        role: this.data?.user?.role,
        legalIdType: this.data?.user?.legalIdType,
        legalId: this.data?.user?.legalId,
        names: this.data?.user?.names,
        lastName: this.data?.user?.lastName,
        email: this.data?.user?.email,
        phone: this.data?.user?.phone,
        birthDate: this.formatDate(birthDay, '-')
    });
  }

  private formatDate(date: any, separator: string): string {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join(separator);
  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

}
