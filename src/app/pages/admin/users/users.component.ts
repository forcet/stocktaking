import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { UserService } from '../services/user.service';
import {MatDialog} from '@angular/material/dialog';
import { ModalComponent } from '../components/modal/modal.component';
import { AlertsComponent } from '@app/shared/components/modal/alerts/alerts.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Person } from '@app/shared/models/user.interface';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, AfterViewInit, OnDestroy {
  
  displayedColumns: string[] = ['legalId', 'names', 'lastName', 'username', 'email', 'phone', 'actions'];
  dataSource = new MatTableDataSource<Person>();

  @ViewChild(MatSort) sort!: MatSort;

  private destroy$ = new Subject<any>();

  ngOnInit(): void {
    this.refresh();
  }

  constructor(private userSrv: UserService, private dialog: MatDialog) {
    
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  onOpenModal(user = {}): void {
    this.dialog.open(ModalComponent, {
      height:'800px',
      width:'600px',
      hasBackdrop: false,
      data: {title: 'Nuevo Usuario', user}
    }).
    afterClosed().subscribe(() => this.refresh());
  }

  onDelete(username : string):void {
    this.dialog.open(AlertsComponent, {
      hasBackdrop: false,
      data: {title: 'Eliminar Usuario', code: username}
    }).afterClosed().subscribe((res) => {
        if(res) { 
          this.userSrv
            .delete(username)
            .pipe(takeUntil(this.destroy$))
            .subscribe(() => this.refresh());
        }
    });

  }

  ngOnDestroy(): void {
    this.destroy$.next({});
    this.destroy$.complete();
  }

  refresh(): void {
    this.userSrv.getAll().subscribe((res) => {
      this.dataSource =  new MatTableDataSource<Person>(res);;
      this.dataSource.sort = this.sort;
    });
  }

}
